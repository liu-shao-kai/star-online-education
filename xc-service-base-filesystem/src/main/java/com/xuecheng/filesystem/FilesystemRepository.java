package com.xuecheng.filesystem;

import com.xuecheng.framework.domain.filesystem.FileSystem;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FilesystemRepository extends MongoRepository<FileSystem,String> {
}
