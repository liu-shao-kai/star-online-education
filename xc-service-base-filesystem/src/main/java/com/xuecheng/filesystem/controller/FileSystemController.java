package com.xuecheng.filesystem.controller;


import com.xuecheng.api.filesystem.FileSystemControllerApi;
import com.xuecheng.framework.domain.filesystem.response.UploadFileResult;
import com.xuecheng.filesystem.service.FileSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/filesystem")
public class FileSystemController implements FileSystemControllerApi {

    @Autowired
    private FileSystemService fileSystemService;



    @PostMapping("/upload") // /filesystem/upload
    //@Override
    public UploadFileResult uploadFile(MultipartFile file, String filetag, String businesskey, String metadata) {
        return fileSystemService.uploadFile(file,filetag,businesskey,metadata);
    }
}
